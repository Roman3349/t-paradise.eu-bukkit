package eu.luckycraft.plugin;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Chat {

    public static void infoMsg(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.YELLOW + msg);
    }

    public static void successMsg(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.GREEN + msg);
    }

    public static void warningMsg(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.GOLD + msg);
    }

    public static void errorMsg(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.DARK_RED + msg);
    }

    public static void deathMsg(PlayerDeathEvent e, String msg) {
        e.setDeathMessage(ChatColor.YELLOW + msg);
    }

}
