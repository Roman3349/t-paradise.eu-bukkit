package eu.luckycraft.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import eu.luckycraft.plugin.commands.*;
import eu.luckycraft.plugin.listeners.*;
import me.frostbitecz.frameworks.jcommands.CommandManager;

public class Main extends JavaPlugin {

    private static final Logger logger = Logger.getLogger("Minecraft");

    public ArrayList<Player> vanished = new ArrayList<>();
    public ArrayList<Player> godModed = new ArrayList<>();

    public HashMap<String, Location> playersDeaths = new HashMap<>();

    @Override
    public void onEnable() {
        if (getServer().getWorld("world_mine") == null) {
            logger.info("[LuckyCraft] Vytvarim tezici svet!");
            getServer().createWorld(
                    WorldCreator.name("world_mine").environment(Environment.NORMAL).type(WorldType.NORMAL));
        } else {
            logger.info("[LuckyCraft] Nacitam tezici svet!");
        }
        logger.info("[LuckyCraft] Nacitam konfiguraci soubor!");
        saveDefaultConfig();
        getServer().getWorld("world_mine").setFullTime(4000);
        getServer().getWorld("world_mine").setPVP(false);
        getServer().getWorld("world_mine").setSpawnFlags(false, false);
        logger.info("[LuckyCraft] Registruji listenery!");
        getServer().getPluginManager().registerEvents(new EntityListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        logger.info("[LuckyCraft] Registruji prikazy!");
        CommandManager.registerCommands(this, new AdminCommands(this));
        CommandManager.registerCommands(this, new BaseCommands(this));
        CommandManager.registerCommands(this, new SpawnCommands(this));
        CommandManager.registerCommands(this, new VIPCommands(this));
        logger.info("[LuckyCraft] Registruji recepty!");
        Recipes.register();
        logger.info("[LuckyCraft] Plugin byl uspesne spusten!");
    }

    @Override
    public void onDisable() {
        logger.info("[LuckyCraft] Ukladam konfiguraci soubor!");
        saveConfig();
        logger.info("[LuckyCraft] Plugin byl uspesne vypnut!");
    }
}
