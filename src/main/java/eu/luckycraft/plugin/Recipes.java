package eu.luckycraft.plugin;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class Recipes {

    static Server server = Bukkit.getServer();
    
    public static void register() {
        chainmailHelmet();
        chainmailChestplate();
        chainmailLeggings();
        chainmailBoots();
        ironHorseArmor();
        goldHorseArmor();
        diamondHorseArmor();
        saddle();
        leather();
        packedIce();
        record13();
        recordCat();
        recordBlocks();
        recordChirp();
        recordFar();
        recordMall();
        recordMellohi();
        recordStal();
        recordStrad();
        recordWard();
        record11();
        recordWait();
    }

    private static void chainmailHelmet() {
        ItemStack item = new ItemStack(Material.CHAINMAIL_HELMET, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("iii", "i i");
        recipe.setIngredient('i', Material.IRON_FENCE);
        server.addRecipe(recipe);
    }

    private static void chainmailChestplate() {
        ItemStack item = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("i i", "iii", "iii");
        recipe.setIngredient('i', Material.IRON_FENCE);
        server.addRecipe(recipe);
    }

    private static void chainmailLeggings() {
        ItemStack item = new ItemStack(Material.CHAINMAIL_LEGGINGS, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("iii", "i i", "i i");
        recipe.setIngredient('i', Material.IRON_FENCE);
        server.addRecipe(recipe);
    }

    private static void chainmailBoots() {
        ItemStack item = new ItemStack(Material.CHAINMAIL_BOOTS, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("i i", "i i");
        recipe.setIngredient('i', Material.IRON_FENCE);
        server.addRecipe(recipe);
    }

    private static void ironHorseArmor() {
        ItemStack item = new ItemStack(Material.IRON_BARDING, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("  i", "iwi", "iii");
        recipe.setIngredient('i', Material.IRON_INGOT);
        recipe.setIngredient('w', Material.WOOL);
        server.addRecipe(recipe);
    }

    private static void goldHorseArmor() {
        ItemStack item = new ItemStack(Material.GOLD_BARDING, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("  g", "gwg", "ggg");
        recipe.setIngredient('g', Material.GOLD_INGOT);
        recipe.setIngredient('w', Material.WOOL);
        server.addRecipe(recipe);
    }

    private static void diamondHorseArmor() {
        ItemStack item = new ItemStack(Material.DIAMOND_BARDING, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("  d", "dwd", "ddd");
        recipe.setIngredient('d', Material.DIAMOND);
        recipe.setIngredient('w', Material.WOOL);
        server.addRecipe(recipe);
    }

    private static void saddle() {
        ItemStack item = new ItemStack(Material.SADDLE, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("lll", "isi");
        recipe.setIngredient('l', Material.LEATHER);
        recipe.setIngredient('i', Material.IRON_INGOT);
        recipe.setIngredient('s', Material.STRING);
        server.addRecipe(recipe);
    }

    private static void leather() {
        ItemStack item = new ItemStack(Material.LEATHER);
        Material material = Material.ROTTEN_FLESH;
        FurnaceRecipe recipe = new FurnaceRecipe(item, material);
        server.addRecipe(recipe);
    }

    private static void packedIce() {
        ItemStack item = new ItemStack(Material.PACKED_ICE, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ii", "ii");
        recipe.setIngredient('i', Material.ICE);
        server.addRecipe(recipe);
    }
    
    private static void record13() {
        ItemStack item = new ItemStack(Material.GOLD_RECORD, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 11);
        server.addRecipe(recipe);
    }
    
    private static void recordCat() {
        ItemStack item = new ItemStack(Material.GREEN_RECORD, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 2);
        server.addRecipe(recipe);
    }
    
    private static void recordBlocks() {
        ItemStack item = new ItemStack(Material.RECORD_3, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 14);
        server.addRecipe(recipe);
    }
    
    private static void recordChirp() {
        ItemStack item = new ItemStack(Material.RECORD_4, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 1);
        server.addRecipe(recipe);
    }
    
    private static void recordFar() {
        ItemStack item = new ItemStack(Material.RECORD_5, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 10);
        server.addRecipe(recipe);
    }
    private static void recordMall() {
        ItemStack item = new ItemStack(Material.RECORD_6, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 5);
        server.addRecipe(recipe);
    }
    private static void recordMellohi() {
        ItemStack item = new ItemStack(Material.RECORD_7, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 13);
        server.addRecipe(recipe);
    }
    private static void recordStal() {
        ItemStack item = new ItemStack(Material.RECORD_8, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK);
        server.addRecipe(recipe);
    }
    private static void recordStrad() {
        ItemStack item = new ItemStack(Material.RECORD_9, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 15);
        server.addRecipe(recipe);
    }
    private static void recordWard() {
        ItemStack item = new ItemStack(Material.RECORD_10, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 6);
        server.addRecipe(recipe);
    }
    
    private static void record11() {
        ItemStack item = new ItemStack(Material.RECORD_11, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.IRON_INGOT);
        server.addRecipe(recipe);
    }
    
    private static void recordWait() {
        ItemStack item = new ItemStack(Material.RECORD_12, 1);
        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("ooo", "odo", "ooo");
        recipe.setIngredient('o', Material.OBSIDIAN);
        recipe.setIngredient('d', Material.INK_SACK, (byte) 12);
        server.addRecipe(recipe);
    }
}
