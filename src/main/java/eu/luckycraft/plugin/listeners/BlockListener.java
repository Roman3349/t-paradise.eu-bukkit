package eu.luckycraft.plugin.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;

import eu.luckycraft.plugin.Main;

public class BlockListener implements Listener {

    private final Main plugin;

    public BlockListener(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent e) {
        IgniteCause cause = e.getCause();
        if (cause == IgniteCause.LIGHTNING || cause == IgniteCause.LAVA || cause == IgniteCause.SPREAD) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void colorSign(SignChangeEvent e) {
        e.setLine(0, ChatColor.translateAlternateColorCodes('§', e.getLine(0)));
        e.setLine(1, ChatColor.translateAlternateColorCodes('§', e.getLine(1)));
        e.setLine(2, ChatColor.translateAlternateColorCodes('§', e.getLine(2)));
        e.setLine(3, ChatColor.translateAlternateColorCodes('§', e.getLine(3)));
    }

}
