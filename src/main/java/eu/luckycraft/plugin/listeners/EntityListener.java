package eu.luckycraft.plugin.listeners;

import org.bukkit.Material;
import org.bukkit.World.Environment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.inventory.ItemStack;

import eu.luckycraft.plugin.Main;

public class EntityListener implements Listener {

    private final Main plugin;

    public EntityListener(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        Entity entity = e.getEntity();
        EntityType entityType = e.getEntityType();
        Environment environment = entity.getWorld().getEnvironment();
        if (entityType == EntityType.WITHER && environment != Environment.NETHER) {
            e.setCancelled(true);
        } else if (entityType == EntityType.ENDER_DRAGON && environment != Environment.THE_END) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        Entity entity = e.getEntity();
        if (entity instanceof Horse) {
            entity.getWorld().dropItem(entity.getLocation(), new ItemStack(Material.MONSTER_EGG, 1, (byte) 100));
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
    }
}
