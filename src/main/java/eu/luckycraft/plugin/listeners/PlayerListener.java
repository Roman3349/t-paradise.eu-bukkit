package eu.luckycraft.plugin.listeners;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.*;
import org.bukkit.util.Vector;

import eu.luckycraft.plugin.Chat;
import eu.luckycraft.plugin.Main;

public class PlayerListener implements Listener {

    private final Main plugin;

    public PlayerListener(Main instance) {
        plugin = instance;
    }

    private final ArrayList<Player> jumpers = new ArrayList<>();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        String name = player.getName();
        if (name.equalsIgnoreCase("Roman3349")) {
            Chat.infoMsg(player, "Tento server pouziva LuckyCraft v. " + plugin.getDescription().getVersion() + "!");
        }
        for (Player vanishedPlayer : plugin.vanished) {
            player.hidePlayer(vanishedPlayer);
        }
        e.setJoinMessage(ChatColor.YELLOW + "Hrac " + name + " se pripojil.");
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        String name = player.getName();
        plugin.vanished.remove(player);
        e.setQuitMessage(ChatColor.YELLOW + "Hrac " + name + " se odpojil.");
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (e.getTo().getBlock().getType() == Material.ENDER_PORTAL_FRAME) {
            Player player = e.getPlayer();
            player.setVelocity(player.getLocation().getDirection().multiply(3));
            player.setVelocity(new Vector(player.getVelocity().getX(), 2.0D, player.getVelocity().getZ()));
            jumpers.add(player);
        }
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        String msg = e.getMessage();
        e.setMessage(ChatColor.translateAlternateColorCodes('&', msg));
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (e.getCause() == DamageCause.FALL && jumpers.contains(player)) {
                e.setCancelled(true);
                jumpers.remove(player);
            } else if (plugin.godModed.contains(player)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player player = e.getEntity();
        Entity killer = player.getKiller();
        String name = player.getName();
        Location loc = player.getLocation();
        DamageCause cause = player.getLastDamageCause().getCause();
        if (plugin.playersDeaths.containsKey(name)) {
            plugin.playersDeaths.remove(name);
            plugin.playersDeaths.put(name, loc);
        } else {
            plugin.playersDeaths.put(name, loc);
        }

        switch (cause) {
            case BLOCK_EXPLOSION:
                Chat.deathMsg(e, "Hrac " + name + " byl zabit explozi TNT");
                break;
            case CONTACT:
                Chat.deathMsg(e, "Hrac " + name + " se nabodl na kaktus");
                break;
            case CUSTOM:
                Chat.deathMsg(e, "Hrac " + name + " byl zabit");
                break;
            case DROWNING:
                Chat.deathMsg(e, "Hrac " + name + " se utopil");
                break;
            case ENTITY_ATTACK:
                switch (killer.getType()) {
                    case PLAYER:
                        Chat.deathMsg(e, "Hrac " + name + " byl zabit hracem " + killer.getName());
                        break;
                    default:
                        break;
                }
                break;
            case ENTITY_EXPLOSION:

                break;
            case FALL:
                Chat.deathMsg(e, "Hrac " + name + " spadl z vysky");
                break;
            case FALLING_BLOCK:

                break;
            case FIRE:
            case FIRE_TICK:
                Chat.deathMsg(e, "Hrac " + name + " uhorel");
                break;
            case LAVA:
                Chat.deathMsg(e, "Hrac " + name + " uhorel v lave");
                break;
            case LIGHTNING:
                Chat.deathMsg(e, "Hrac " + name + " byl zasazen bleskem");
                break;
            case MAGIC:

                break;
            case MELTING:

                break;
            case POISON:
                Chat.deathMsg(e, "Hrac " + name + " byl otraven");
                break;
            case PROJECTILE:
                switch (killer.getType()) {
                    case PLAYER:
                        Chat.deathMsg(e, "Hrac " + name + " byl zastrelen hracem  " + killer.getName());
                        break;
                    case SKELETON:
                        Chat.deathMsg(e, "Hrac " + name + " byl zastrelen kostlivcem");
                        break;
                    default:
                        break;
                }
                break;
            case STARVATION:
                Chat.deathMsg(e, "Hrac " + name + " umrel hlady");
                break;
            case SUFFOCATION:

                break;
            case SUICIDE:

                break;
            case THORNS:

                break;
            case VOID:
                Chat.deathMsg(e, "Hrac " + name + " spadl do prazdnoty");
                break;
            case WITHER:
                Chat.deathMsg(e, "Hrac " + name + " byl zabit Witherem");
                break;
            default:
                Chat.deathMsg(e, "Hrac " + name + " byl zabit");
                break;
        }
    }

}
