package eu.luckycraft.plugin.commands;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.luckycraft.plugin.Chat;
import eu.luckycraft.plugin.Main;
import me.frostbitecz.frameworks.jcommands.CommandContext;
import me.frostbitecz.frameworks.jcommands.CommandHandler;

public class VIPCommands {

    private final Main plugin;

    public VIPCommands(Main plugin) {
        this.plugin = plugin;
    }

    @CommandHandler(name = "heal", minimumArgs = 0, maximumArgs = 1, description = "Uzdraveni hrace.", usage = "Napis /heal pro sve uzdraveni.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.heal")
    public void healCommand(CommandSender sender, CommandContext args) {
        if (args.getArgs().isEmpty()) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.setHealth(20);
                Chat.successMsg(player, "Byl jste uspesne vylecen.");
            } else {
                Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
            }
        } else {
            String name = args.getArgs().get(0);
            Player target = plugin.getServer().getPlayer(name);
            if (target != null) {
                target.setHealth(20);
                Chat.successMsg(sender, "Hrac " + name + " byl vylecen.");
                Chat.successMsg(sender, "Hrac " + name + " byl vylecen.");
                if (sender instanceof Player) {
                    Chat.infoMsg(target, "Byl jste vylecen hracem " + sender.getName() + ".");
                } else {
                    Chat.infoMsg(target, "Byl jste vylecen.");
                }
            } else {
                Chat.errorMsg(sender, "Hrac " + name + "neni online");
            }
        }
    }

    @CommandHandler(name = "feed", minimumArgs = 0, maximumArgs = 1, description = "Najezeni hrace.", usage = "Napis /heal pro sve najezeni.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.feed")
    public void feedCommand(CommandSender sender, CommandContext args) {
        if (args.getArgs().isEmpty()) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.setFoodLevel(20);
                Chat.successMsg(player, "Byl jste uspesne najezen.");
            } else {
                Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
            }
        } else {
            String name = args.getArgs().get(0);
            Player target = plugin.getServer().getPlayer(name);
            if (target != null) {
                target.setFoodLevel(20);
                Chat.successMsg(sender, "Hrac " + name + " byl nakrmen.");
                if (sender instanceof Player) {
                    Chat.infoMsg(target, "Byl jste nakrmen hracem " + sender.getName() + ".");
                } else {
                    Chat.infoMsg(target, "Byl jste nakrmen.");
                }
            } else {
                Chat.errorMsg(sender, "Hrac " + name + "neni online");
            }
        }
    }

    @CommandHandler(name = "bed", minimumArgs = 0, maximumArgs = 0, description = "Teleportace k posteli.", usage = "Napis /bed pro teleportaci ke své posteli.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.bed")
    public void bedCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location locBed = player.getBedSpawnLocation();
            if (locBed == null) {
                Chat.successMsg(player, "Vase postel nebyla nalezena.");
            } else {
                player.teleport(locBed);
                Chat.successMsg(player, "Byl jste uspesne teleportovan ke sve posteli.");
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "ptime", minimumArgs = 0, maximumArgs = 2, description = "Nastaveni uzivatelskeho casu.", usage = "Napis /ptime pro nastavení sveho herniho casu.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.ptime")
    public void ptimeCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            switch (args.getArgs().get(0)) {
                case "set":
                    int time = Integer.parseInt(args.getArgs().get(1));
                    if (time >= 0 && time <= 24000) {
                        player.setPlayerTime(time, false);
                        Chat.successMsg(player, "Uspesne jste si nastavili herni cas na " + args.getArgs().get(1) + ".");
                    } else {
                        Chat.errorMsg(player, "Tato hodnota nelze nastavit.");
                        Chat.errorMsg(player, "Lze pouze nastavit hodnota z intervalu <0;24000>.");
                    }
                    break;
                case "reset":
                    player.resetPlayerTime();
                    Chat.successMsg(player, "Vas herni cas byl uspesne vyresetovan.");
                    break;

                default:
                    break;
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "enderchest", aliases = {"chest"}, minimumArgs = 0, maximumArgs = 1, description = "Otevreni ender chest.", usage = "Napis /enderchest pro otevreni ender chest.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.enderchest")
    public void enderchestCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.getArgs().isEmpty()) {
                player.openInventory(player.getEnderChest());
                Chat.successMsg(player, "Uspesne jste otevreli svoji enderchest.");
            } else {
                if (player.hasPermission("luckycraft.cmd.enderchest.admin")) {
                    String name = args.getArgs().get(0);
                    Player target = plugin.getServer().getPlayer(name);
                    if (target == null) {
                        Chat.errorMsg(sender, "Hrac " + name + "neni online");
                    } else {
                        player.openInventory(target.getEnderChest());
                        Chat.successMsg(player, "Uspesne jste otevreli enderchest hrace " + name + ".");
                    }
                }
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "crafting", aliases = {"craft", "workbench"}, minimumArgs = 0, maximumArgs = 0, description = "Otevreni crafting table.", usage = "Napis /crafting pro otevreni crafting table.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.crafting")
    public void craftingCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.openWorkbench(null, true);
            Chat.successMsg(player, "Uspesne jste otevreli crafting table.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "fly", minimumArgs = 0, maximumArgs = 1, description = "Zapnuti/vypnuti letani.", usage = "Napis /fly pro zapnuti/vypnuti letani.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.fly")
    public void flyCommand(CommandSender sender, CommandContext args) {
        Player player = (Player) sender;
        if (args.getArgs().isEmpty()) {
            if (sender instanceof Player) {
                if (player.getAllowFlight()) {
                    player.setAllowFlight(false);
                    Chat.successMsg(player, "Vypnul jste si fly mod.");
                } else {
                    player.setAllowFlight(true);
                    Chat.successMsg(player, "Zapnul jste si fly mod.");
                }
            } else {
                Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
            }
        } else if (sender.hasPermission("luckycraft.cmd.fly.admin")) {
            String name = args.getArgs().get(0);
            Player target = plugin.getServer().getPlayer(name);
            if (target == null) {
                Chat.errorMsg(sender, "Hrac " + name + "neni online");
            } else {
                if (target.getAllowFlight()) {
                    target.setAllowFlight(false);
                    Chat.successMsg(target, "Byl Vam vypnut fly mod.");
                    Chat.successMsg(sender, "Vypnul jste hraci " + name + " fly mod.");
                } else {
                    target.setAllowFlight(true);
                    Chat.successMsg(target, "Byl Vam zapnut fly mod.");
                    Chat.successMsg(sender, "Zapnul jste hraci " + name + " fly mod.");
                }
            }
        } else {
            Chat.errorMsg(sender, "Pouziti: /fly [jmeno]");
        }
    }

    @CommandHandler(name = "death", minimumArgs = 0, maximumArgs = 0, description = "Teleportace na misto posledni smrti.", usage = "Napis /death pro teleportaci na misto posledni smrti.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.death")
    public void deathCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String name = player.getName();
            if (plugin.playersDeaths.containsKey(name)) {
                player.teleport(plugin.playersDeaths.get(name));
                Chat.successMsg(player, "Byl jste uspesne teleportovan na misto sve posledni srmti.");
            } else {
                Chat.errorMsg(player, "Zatim jste neumreli.");
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }
}
