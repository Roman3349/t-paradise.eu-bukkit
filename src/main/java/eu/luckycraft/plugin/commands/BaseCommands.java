package eu.luckycraft.plugin.commands;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import eu.luckycraft.plugin.Chat;
import eu.luckycraft.plugin.Main;
import me.frostbitecz.frameworks.jcommands.CommandContext;
import me.frostbitecz.frameworks.jcommands.CommandHandler;

public class BaseCommands {

    private final Main plugin;

    public BaseCommands(Main plugin) {
        this.plugin = plugin;
    }

    @CommandHandler(name = "mine", aliases = {"tezba"}, minimumArgs = 0, maximumArgs = 0, description = "Teleportace do teziciho sveta.", usage = "Napis /mine pro teleportaci do tezebniho sveta.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.mine")
    public void mineCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location loc = plugin.getServer().getWorld("world_mine").getSpawnLocation();
            player.teleport(loc);
            Chat.successMsg(player, "Byl jste uspesne teleportovan do teziciho sveta.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "nether", aliases = {"peklo"}, minimumArgs = 0, maximumArgs = 0, description = "Teleportace do netheru.", usage = "Napis /nether pro teleportaci do netheru.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.nether")
    public void netherCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location loc = plugin.getServer().getWorld("world_nether").getSpawnLocation();
            player.teleport(loc);
            Chat.successMsg(player, "Byl jste uspesne teleportovan do netheru.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "end", aliases = {"the_end"}, minimumArgs = 0, maximumArgs = 0, description = "Teleportace do endu.", usage = "Napis /end pro teleportaci do endu.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.end")
    public void endCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Location loc = plugin.getServer().getWorld("world_the_end").getSpawnLocation();
            player.teleport(loc);
            Chat.successMsg(player, "Byl jste uspesne teleportovan do endu.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "rules", aliases = {"pravidla"}, minimumArgs = 0, maximumArgs = 0, description = "Zobrazeni pravidel serveru.", usage = "Napis /rules pro zobrazeni pravidel.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.rules")
    public void rulesCommand(CommandSender sender, CommandContext args) {
        Chat.warningMsg(sender, "Pravidla naleznete na http://luckycraft.8u.cz/pravidla.php");
    }
}
