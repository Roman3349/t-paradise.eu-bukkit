package eu.luckycraft.plugin.commands;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.command.BlockCommandSender;

import eu.luckycraft.plugin.Chat;
import eu.luckycraft.plugin.Main;
import me.frostbitecz.frameworks.jcommands.CommandContext;
import me.frostbitecz.frameworks.jcommands.CommandHandler;

public class AdminCommands {

    private final Main plugin;

    public AdminCommands(Main plugin) {
        this.plugin = plugin;
    }

    @CommandHandler(name = "vanish", minimumArgs = 0, maximumArgs = 0, description = "Z(ne)viditelneni.", usage = "Napis /vanish pro z(ne)viditelneni.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.vanish")
    public void vanishCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!plugin.vanished.contains(player)) {
                for (Player players : plugin.getServer().getOnlinePlayers()) {
                    players.hidePlayer(player);
                }
                plugin.vanished.add(player);
                Chat.successMsg(player, "Nyni jste neviditelny!");
            } else {
                for (Player players : plugin.getServer().getOnlinePlayers()) {
                    players.showPlayer(player);
                }
                plugin.vanished.remove(player);
                Chat.successMsg(player, "Nyni jste viditelny!");
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "invsee", aliases = {"inventory"}, minimumArgs = 0, maximumArgs = 1, description = "Zobrazeni inventare hrace.", usage = "Napis /invsee <hrac> pro zobrazeni inventare hrace.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.invsee")
    public void invseeCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.getArgs().isEmpty()) {
                Chat.errorMsg(player, "Pouziti /invsee <hrac>");
            } else {
                String name = args.getArgs().get(0);
                Player target = plugin.getServer().getPlayer(name);
                if (target == null) {
                    Chat.errorMsg(player, "Hrac " + name + "neni online");
                } else {
                    player.openInventory(target.getInventory());
                    Chat.successMsg(player, "Nyni prohlizite inventar hrace " + name + ".");
                }
            }
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "kill", minimumArgs = 0, maximumArgs = 1, description = "Zabiti hrace.", usage = "Napis /kill <hrac> pro zabiti hrace.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.kill")
    public void killCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof BlockCommandSender) {
            BlockCommandSender cmdBlock = (BlockCommandSender) sender;
            cmdBlock.getBlock().setType(Material.AIR);
        } else {
            if (args.getArgs().isEmpty()) {
                Chat.errorMsg(sender, "Pouziti /kill <hrac>");
            } else {
                String name = args.getArgs().get(0);
                Player target = plugin.getServer().getPlayer(name);
                if (target == null) {
                    Chat.errorMsg(sender, "Hrac " + name + "neni online");
                } else {
                    target.setHealth(0);
                    Chat.successMsg(sender, "Hrac " + name + " byl uspesne zabit.");
                }
            }
        }
    }

    @CommandHandler(name = "god", minimumArgs = 0, maximumArgs = 0, description = "Vypnuti/zapnuti nesmrtelnosti.", usage = "Napis /god pro vypnuti/zapnuti nesmrtelnosti.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.god")
    public void godCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (plugin.godModed.contains(player)) {
                plugin.godModed.remove(player);
                Chat.successMsg(player, "Nyni jste smrtelni.");
            } else {
                plugin.godModed.add(player);
                Chat.successMsg(player, "Nyni jste nesmrtelni.");
            }
        }
    }
}
