package eu.luckycraft.plugin.commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;

import eu.luckycraft.plugin.Chat;
import eu.luckycraft.plugin.Main;
import me.frostbitecz.frameworks.jcommands.CommandContext;
import me.frostbitecz.frameworks.jcommands.CommandHandler;

public class SpawnCommands {

    private final Main plugin;

    public SpawnCommands(Main plugin) {
        this.plugin = plugin;
    }

    @CommandHandler(name = "spawn", minimumArgs = 0, maximumArgs = 0, description = "Teleportace na spawn.", usage = "Napis /spawn pro teleportaci na spawn.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.spawn")
    public void spawnCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Configuration cfg = plugin.getConfig();
            double x = cfg.getDouble("spawn.x");
            double y = cfg.getDouble("spawn.y");
            double z = cfg.getDouble("spawn.z");
            World world = plugin.getServer().getWorld(cfg.getString("spawn.world"));
            float yaw = (float) cfg.getInt("spawn.yaw");
            float pitch = (float) cfg.getInt("spawn.pitch");
            player.teleport(new Location(world, x, y, z, yaw, pitch));
            Chat.successMsg(player, "Byl jste uspesne teleportovan na spawn.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

    @CommandHandler(name = "setspawn", minimumArgs = 0, maximumArgs = 0, description = "Nastavení spawnu.", usage = "Napis /spawn pro nastaveni spawnu.", permissionMessage = "Nemate pravomoce k pouziti tohoto prikazu!", permission = "luckycraft.cmd.setspawn")
    public void setspawnCommand(CommandSender sender, CommandContext args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Configuration cfg = plugin.getConfig();
            Location loc = player.getLocation();
            cfg.set("spawn.x", loc.getX());;
            cfg.set("spawn.y", loc.getY());
            cfg.set("spawn.z", loc.getZ());
            cfg.set("spawn.world", loc.getWorld().getName());
            cfg.set("spawn.yaw", loc.getYaw());
            cfg.set("spawn.pitch", loc.getPitch());
            Chat.successMsg(player, "Uspesne jste nastavili spawn.");
        } else {
            Chat.errorMsg(sender, "Tento prikaz muze pouzit pouze hrac.");
        }
    }

}
